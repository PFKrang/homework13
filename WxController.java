import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class WxController implements Initializable {

  @FXML
  private Button btngo;

  @FXML
  private TextField txtzipcode;

  @FXML
  private Label lbltime;
  
  @FXML
  private Label lblcity;

  @FXML
  private Label lbltemp;

  @FXML
  private Label lblwind;
  
  @FXML
  private Label lblpres;
  
  @FXML
  private Label lblvisi;
  
  @FXML
  private Label lblweat;
  
  @FXML
  private ImageView iconwx;

  @FXML
  private void handleButtonAction(ActionEvent e) {
    // Create object to access the Model
    WxModel weather = new WxModel();

    // Has the go button been pressed?
    if (e.getSource() == btngo)
    {
      String zipcode = txtzipcode.getText();
      if (weather.getWx(zipcode))
      {
        lblcity.setText(weather.getLocation());
        lbltime.setText(weather.getTime());
        lbltemp.setText(String.valueOf(weather.getTemp()));
        lblwind.setText(weather.getWind());
        lblpres.setText(String.valueOf(weather.getPressure()));
        lblvisi.setText(String.valueOf(weather.getVisibility()));
        lblweat.setText(weather.getWeather());
        iconwx.setImage(weather.getImage());
      }
      else
      {
        lblcity.setText("Invalid Zipcode");
        lbltemp.setText("");
        iconwx.setImage(new Image("badzipcode.png"));
      }
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
	  //TODO
  }    

}
